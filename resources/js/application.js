jQuery(document).ready(function($) {
	
	/**BUTTON BACK TO TOP**/
	$(window).scroll(function() {
		if($(window).scrollTop() >= 500)
		{
			$('#to_top').fadeIn();
			// $('.header.active-pc').addClass('bg-scroll');
		}
		else
		{
			$('#to_top').fadeOut();
			// $('.header.active-pc').removeClass('bg-scroll');
		}
	});

	$("#to_top,.on_top").click(function() {
		$("html, body").animate({ scrollTop: 0 });
		return false;
	});
	/**END BUTTON BACK TO TOP**/

});

// $(function() {
// 	$('.img-lazy').lazy({
// 		threshold: 100,
// 		effect : "fadeIn"
// 	});
// });

// OWL CAROUSEL
// $('.slider-top .owl-carousel').owlCarousel({
// 	loop: true,
// 	autoplay:true,
// 	autoplayTimeout:5000,
// 	autoplayHoverPause: true,
// 	responsiveClass: true,
// 	dots: true,
// 	nav: true,
// 	items: 1
// })

// $('.feedback .owl-carousel').owlCarousel({
// 	loop:true,
// 	margin:20,	
// 	autoplay:true,
// 	autoplayTimeout:5000,
// 	nav:false,
// 	dots: true,
// 	responsive:{
// 		0:{
// 			items:1
// 		},
// 		768:{
// 			items:2
// 		},
// 		992:{
// 			items:3
// 		},
// 		1024:{
// 			items:3
// 		},
// 		1200:{
// 			items:4
// 		},
// 		1366:{
// 			items:4
// 		}
// 	}
// })

// END OWL CAROUSEL
